package com.lw.fx.comm.entity.log;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * gate_log
 * @author 
 */
@Data
public class GateLog implements Serializable {
    private Integer id;

    private String menu;

    private String opt;

    private String uri;

    private Date crtTime;

    private String crtUser;

    private String crtName;

    private String crtHost;

    private String body;

    private static final long serialVersionUID = 1L;
}