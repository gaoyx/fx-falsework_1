package com.fx.server.base;

import lombok.Data;

import java.util.Date;

/**
 * 基础实体类
 *
 */
@Data
public class BaseEntity implements java.io.Serializable {

    /**
     * 主键
     */
    private String id;

    /**
     * 唯一标识,防止重复，也防止按照ID拖库
     */
    private String uuid;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 版本
     */
    private Integer version;

}
