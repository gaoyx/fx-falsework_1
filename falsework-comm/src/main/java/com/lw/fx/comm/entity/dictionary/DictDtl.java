package com.lw.fx.comm.entity.dictionary;

import com.lw.fx.comm.base.BaseEntity;
import lombok.Data;

/**
 * @description:字典分组类
 */
@Data
public class DictDtl extends BaseEntity {

    /**
     * 字典类型ID
     */
    private String headId;

    /**
     * 明细名称
     */
    private String dtlName;

    /**
     * 明细编码
     */
    private String dtlCode;

    /**
     * 明细描述
     */
    private String dtlDesc;

    /**
     * 排序
     */
    private String sortOrder;

    /**
     * 特殊标志位，用于后期扩展
     */
    private String dtlFlag;
}
