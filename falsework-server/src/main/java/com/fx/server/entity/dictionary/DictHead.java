package com.fx.server.entity.dictionary;

import com.fx.server.base.BaseEntity;
import lombok.Data;

/**
 * @description:字典明细
 */
@Data
public class DictHead extends BaseEntity {

    /**
     * 字典名称
     */
    private String headName;

    /**
     * 字典编码
     */
    private String headCode;

    /**
     * 字典描述
     */
    private String headDesc;
}
