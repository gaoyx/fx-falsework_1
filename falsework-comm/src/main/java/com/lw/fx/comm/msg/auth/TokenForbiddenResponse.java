package com.lw.fx.comm.msg.auth;


import com.lw.fx.comm.constant.RestCodeConstants;
import com.lw.fx.comm.msg.BaseResponse;

/**
 *
 * @Description:
 *
 * @param:
 * @return:
 * @auther: liwen
 * @date: 2020/8/2 10:56 上午
 */
public class TokenForbiddenResponse  extends BaseResponse {
    public TokenForbiddenResponse(String message) {
        super(RestCodeConstants.TOKEN_FORBIDDEN_CODE, message);
    }
}
