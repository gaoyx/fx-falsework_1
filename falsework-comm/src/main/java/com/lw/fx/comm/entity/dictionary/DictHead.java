package com.lw.fx.comm.entity.dictionary;

import com.lw.fx.comm.base.BaseEntity;
import lombok.Data;

/**
 * @description:字典明细
 */
@Data
public class DictHead extends BaseEntity {

    /**
     * 字典名称
     */
    private String headName;

    /**
     * 字典编码
     */
    private String headCode;

    /**
     * 字典描述
     */
    private String headDesc;
}
