package com.lw.fx.comm.vo;


import java.util.List;

/**
 *
 * @Description:
 *
 * @param:
 * @return:
 * @auther: liwen
 * @date: 2020/8/2 10:56 上午
 */
public class GroupUsersVO {
    List<UserVO> members;
    List<UserVO> leaders;
    List<UserVO> users;

    public GroupUsersVO() {
    }

    public GroupUsersVO(List<UserVO> members, List<UserVO> leaders, List<UserVO> users) {
        this.members = members;
        this.leaders = leaders;
        this.users = users;
    }

    public List<UserVO> getMembers() {
        return members;
    }

    public void setMembers(List<UserVO> members) {
        this.members = members;
    }

    public List<UserVO> getLeaders() {
        return leaders;
    }

    public void setLeaders(List<UserVO> leaders) {
        this.leaders = leaders;
    }

    public List<UserVO> getUsers() {
        return users;
    }

    public void setUsers(List<UserVO> users) {
        this.users = users;
    }
}
